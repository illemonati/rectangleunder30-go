GOCMD = go
BINARY_NAME = "rectangleUnder30-go"

build:
	$(GOCMD) build -ldflags="-s -w" -o $(BINARY_NAME) ./src/

run:
	./$(BINARY_NAME)

all: build run
