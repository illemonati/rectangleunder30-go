package main

import (
	"fmt"
	"math"
	"os"
	"sort"
)

// MaxNumber : max side length
const MaxNumber int = 30


// FindSingle : find a single side length
func FindSingle(n1, n2 int) int {
	return int(math.Abs(float64(n1 - n2)))
}

// Process : process a rect config
func Process(a, b, c, d, current int) int {
	if (a == 0) && (b == 0) && (c == 0) && (d == 0) {
		return current
	}
	return Process(FindSingle(a, b), FindSingle(b, c), FindSingle(c, d), FindSingle(d, a), current+1)
}

func writeFile(save map[int][]string) {
	file, _ := os.Create("save.txt")
	layerss := make([]int, len(save))
	for lys := range save {
		layerss = append(layerss, lys)
	}
	sort.Ints(layerss)
	for _, layers := range layerss {
		configs := save[layers]
		for _, config := range configs {
			file.WriteString(fmt.Sprintf("%d : %s\n", layers, config))
		}
	}
	file.Sync()
}

func main() {
	save := make(map[int][]string)
	maxNum := MaxNumber + 1
	for i := 1; i < maxNum; i++ {
		for j := 1; j < maxNum; j++ {
			for k := 1; k < maxNum; k++ {
				for l := 1; l < maxNum; l++ {
					layers := Process(i, j, k, l, 0)
					config := fmt.Sprintf("(%d, %d, %d, %d)", i, j, k, l)
					save[layers] = append(save[layers], config)
				}
			}
		}
	}
	writeFile(save)
}
